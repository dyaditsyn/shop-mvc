<?php

namespace Shop\Views;

class View
{
    private $ext = 'php';
    private $folder = "templates";
    private $header = "header";
    private $footer = "footer";

    public function setExt(string $ext)
    {
        $this->ext = $ext;
    }

    public function setViewsFolder(string $dir)
    {
        $this->folder = $dir;
    }

    public function render(string $viewName, array $data = [])
    {
        if (!empty($data)) {
            extract($data); // ключи массива становятся названиями переменных а параметры значениями переменных - все продукты для отображения
        }
        require ROOT_PATH . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . $this->header . "." . $this->ext;
        require ROOT_PATH . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . $viewName . "." . $this->ext;
        require ROOT_PATH . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . $this->footer . "." . $this->ext;
    }

    public function renderFull(string $viewName, array $data = [])
    {
        if (!empty($data)) {
            extract($data); // ключи массива становятся названиями переменных а параметры значениями переменных - все продукты для отображения
        }
        require ROOT_PATH . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . $viewName . "." . $this->ext;
    }
}
