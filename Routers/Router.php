<?php

namespace Shop\Routers;

class Router
{
    private $routes = [];
    public static $currentAction;

    public function __construct()
    {
        $this->init();
    }

    public static function redirect($url)
    {
        header("Location: " . $url);
        die();
    }

    public function init()
    {
        $this->setRoute("/", "Product", "index");
        $this->setRoute("login", "User", "login");
        $this->setRoute("register", "User", "register");
        $this->setRoute("logout", "User", "logout");
        $this->setRoute("cart", "Cart", "index");
        $this->setRoute("cart/add", "Cart", "add");
    }

    public function setRoute(string $url, string $controller, string $action): bool
    {
        if (empty($this->routes[$url])) {
            $this->routes[$url] = ["controller" => $controller, "action" => $action];
            return true;
        }
        return false;
    }

    public function getRoute(string $url): array
    {
        return $this->routes[$url] ?? [];
    }

    public function process(string $url, array $request)
    {
        $route = $this->getRoute($url);
        if (empty($route)) {
            // 404 error
            $controller = new \Shop\Controllers\ErrorController();
            $controller->error404($request);
            return;
        }
        self::$currentAction = $route['action'];
        $controllerName = "\\Shop\\Controllers\\" . $route['controller'] . "Controller";
        $controller = new $controllerName();
        $controller->{$route['action']}($request); // ProductController->index($requst)
    }
}
