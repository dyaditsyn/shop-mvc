<?php
define("ROOT_PATH", dirname(__FILE__, 2));
require_once ROOT_PATH . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use \Shop\Routers\Router;

$dotenv = Dotenv\Dotenv::createImmutable(ROOT_PATH);
$dotenv->load();
session_start();

$request = $_REQUEST ?? [];
$path = $request['path'] ?? "/";
unset($request['path']);
$router = new Router();
$router->process($path, $request);
