<?php

namespace Shop\Controllers;

use Shop\Views\View;

class BaseController
{
    public $view;
    protected $exeption = [];

    public function __construct()
    {
        $this->view = new View();
    }
}
