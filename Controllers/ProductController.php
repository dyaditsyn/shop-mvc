<?php

namespace Shop\Controllers;

use Shop\Models\Product;
use Shop\Routers\Router;

class ProductController extends BaseController
{
    public function __construct()
    {
        if (empty($_SESSION['user_id'])) {
            Router::redirect("/login");
        }
        parent::__construct();
    }

    public function index(array $request = null)
    {
        $products = Product::getAll();
        $this->view->render('products', ["products" => $products]);
    }
}
