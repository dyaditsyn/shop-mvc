<?php

namespace Shop\Controllers;

use Shop\Models\Product;
use Shop\Models\Cart;
use Shop\Models\CartProduct;
use Shop\Routers\Router;

class CartController extends BaseController
{
    protected $exception = ['add'];

    public function __construct()
    {
        if (empty($_SESSION['user_id'])) {
            Router::redirect("/login");
        }
        if (empty($_SESSION['cart_id']) && !in_array(Router::$currentAction, $this->exception)) {
            Router::redirect("/");
        }
        parent::__construct();
    }

    public function index(array $request = null)
    {
        $cart = Cart::getCartById($_SESSION['cart_id']);
        $products = $cart->getProducts();
        $totalPrice = $cart->calcTotalProductPrice();
        $totalPrice = number_format($totalPrice, 2, '.', ',');
        $this->view->render('cart', ["products" => $products, "totalPrice" => $totalPrice]);
    }

    public function add(array $request = null)
    {
        if (!empty($request['products'])) {
            if (!empty($_SESSION['cart_id'])) {
                $cart = Cart::getCartById($_SESSION['cart_id']);
                $cart->clearCart();
            } else {
                $cart = Cart::create($_SESSION['user_id']);
                $_SESSION['cart_id'] = $cart->getId();
            }
            $cartProducts = [];
            foreach ($request['products'] as $product) {
                $quantity = $request['quantity_' . $product];
                $cartProducts[] = CartProduct::create($_SESSION['cart_id'], (int) $product, (int) $quantity);
            }
            $cart->setProducts($cartProducts);
            Router::redirect("/cart");
        }
    }
}
